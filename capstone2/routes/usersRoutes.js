const express = require("express");
const usersControllers = require('../controllers/usersControllers.js')

const auth = require("../auth.js")

const router = express.Router();


// Routes

// route for user registration
router.post("/register", usersControllers.registerUser);

// route for user authentication
router.post("/login", usersControllers.loginUser);

// Route for creating order
router.post('/createOrder', auth.verify, usersControllers.createOrder)

// Route for retrieving user details
router.get("/details", auth.verify, usersControllers.getProfile);

// Route for Retrieving all orders (Admin only)
router.get("/allOrders", auth.verify, usersControllers.getAllOrders);


// Route for Retrieving user's orders (Admin only)
router.get("/userOrders", auth.verify, usersControllers.getAllOrders);

module.exports = router;