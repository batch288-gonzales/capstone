const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
	firstName:{
		type: String,
		required: [true, "User First Name is required!"]
	},

	lastName:{
		type: String,
		required: [true, "User Last Name is required!"]
	},

	email:{
		type: String,
		required: [true, "User email is required!"]
	},

	password:{
		type: String,
		required: [true, "User password is required!"]
	},

	isAdmin:{
		type: Boolean,
		default: false
	},

	orderedProducts: [
			{
				products: {
						productId: {
						type: String,
						required: [true, "productId is required!"]
					},
						productName: {
						type: String,
						required: [true, "productName is required!"]
					},
						quantity: {
						type: Number,
						required: [true, "quantity is required!"]
					}
				},

				totalAmount: {
						type: Number
				},

				purchasedOn: {
						type: Date,
						default: new Date()
				},

				status: {
					type: String,
					default: "Order Placed"
				}
			}

		]

})

const Users = mongoose.model("User", userSchema);

module.exports = Users;