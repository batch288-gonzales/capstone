const Products = require('../models/Products.js');

const auth = require('../auth.js')


// Controller for Creating Product (Admin Only)
module.exports.addProducts = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin){
	
		let newProduct = new Products({
			name: request.body.name,
			description: request.body.description,
			price: request.body.price,
			isActive: request.body.isActive,
			stock: request.body.stock,
			category: request.body.category
		})

		newProduct.save()
		.then(save => response.send(true))
		.catch(error => response.send(false))
	}else{
		return response.send(false);
	}

}


// Controller for retrieving all products (Admin Only)
module.exports.getAllProducts = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin){
		Products.find({})
		.then( result => response.send(result))
		.catch( error => response.send(false));
	}else{
		return response.send(false);
	}

}


// Controller for retrieving all active products only
module.exports.getActiveProducts = (request, response) => {

	Products.find({isActive: true})
	.then(result => response.send(result))
	.catch( error => response.send(error));

}


// Controller for retrieving single product
module.exports.getProduct = (request, response) => {

	const productId = request.params.productId;
	Products.findById(productId)
	.then(result => response.send(result))
	.catch(error => response.send(error))

}


// Controller for updating product informations
module.exports.updateProduct = (request, response) => {

	const userData = auth.decode(request.headers.authorization);
	const productId = request.params.productId;

	let updatedProduct = {
		name: request.body.name,
		description: request.body.description,
		price: request.body.price,
		isActive: request.body.isActive,
		stock: request.body.stock,
		category: request.body.category
	}

	if(userData.isAdmin){
		Products.findByIdAndUpdate(productId, updatedProduct)
		.then(result => response.send(true))
		.catch(error => response.send(false))
	}else{
		return response.send(false)
	}

}



// Controller for Archive Product (Admin only)
module.exports.archiveProduct = (request, response) => {

	const userData = auth.decode(request.headers.authorization);
	const productId = request.params.productId;

	let archivedProduct = {
		isActive: request.body.isActive
	}

	if(userData.isAdmin){
		Products.findByIdAndUpdate(productId, archivedProduct)
		.then(result => response.send(true))
		.catch(error => response.send(false))
	}else{
		return response.send(false)
	}
}


// Controller for Activate Product (Admin only)
module.exports.activateProduct = (request, response) => {

	const userData = auth.decode(request.headers.authorization);
	const productId = request.params.productId;

	let archivedProduct = {
		isActive: request.body.isActive
	}

	if(userData.isAdmin){
		Products.findByIdAndUpdate(productId, archivedProduct)
		.then(result => response.send(true))
		.catch(error => response.send(false))
	}else{
		return response.send(false)
	}
}


