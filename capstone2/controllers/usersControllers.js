const Users = require("../models/Users.js");
const Products = require("../models/Products.js");

const bcrypt =require("bcrypt");

const auth = require('../auth.js');


// Controller for User Registration
module.exports.registerUser = (request, response) => {
	const { password, retypePassword} = request.body;

	if (password !== retypePassword) {
    return response.json(false);
  }

  Users.findOne({ email: request.body.email })
    .then(result => {
      if (result) {
        response.json(false);
      } else {
        let newUser = new Users({
          firstName: request.body.firstName,
          lastName: request.body.lastName,
          email: request.body.email,
          password: bcrypt.hashSync(request.body.password, 10),
          isAdmin: request.body.isAdmin
        });

        newUser.save()
          .then(saved => response.json(true))
          .catch(error => response.json(false));
      }
    })
    .catch(error => response.json(false));
};



// Controller for User Authentication
module.exports.loginUser = (request, response) => {

	Users.findOne({email : request.body.email})

	.then(result => {
		console.log('Result:', result);


		if(!result){
			return response.send(false)
		}else{
			
			let isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

			if(isPasswordCorrect){
				return response.send({
					auth: auth.createAccessToken(result)
				})
				
			}else{
				return response.send(false)
			}
		}

	})
	.catch(error => response.send(false));

}


// Controller for Non-admin User checkout (Create Order)
module.exports.createOrder = (request, response) => {

	const productId = request.body.id;
	const productName = request.body.name;
	const quantity = request.body.quantity;
	const userData = auth.decode(request.headers.authorization);
	const price = request.body.price
	const totalAmount = quantity * price
	

	if(userData.isAdmin){
		return response.send(false)
	}else{

		// Push through user document
	let isUserUpdated = Users.findOne({_id : userData.id})
		.then(result => {
			result.orderedProducts.push({products : {productId : productId, productName : productName, quantity : quantity}, totalAmount : totalAmount})

			result.save()
			.then(saved = true)
			.catch(error => false)
		})
		.catch(error => false)



		// Push through product document
	let isProductUpdated = Products.findOne({_id : productId})
		.then(result => {
			result.userOrders.push({userId : userData.id})
			

		result.save()
		.then(saved => true)
		.catch(error => false)

		})
		.catch(error => false)


		if(isUserUpdated && isProductUpdated){
			return response.send(true)
		}else{
			return response.send(false)
		}

	}

}


// Controller for Retrieving User Details (Admin Only)
module.exports.getProfile = (request, response) => {

	const userData = auth.decode(request.headers.authorization)

	Users.findOne({_id : userData.id})
	.then(result => {
		
		password : result.password = "Confidential"

		return response.send(result)
	})
	.catch(error => response.send(error));

}



// Stretch Goals

// Controller for Retrieving all orders (Admin only)
module.exports.getAllOrders = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin){
		Users.find({"orderedProducts.status" : "Order Placed"})
		.then(result => {
		
		password : result.password = ""

		return response.send(result)
	})
	.catch(error => response.send(error));

	}else{
		return response.send(`You don't have access to this route.`);
	}

}


















